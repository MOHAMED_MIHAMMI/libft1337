/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: momihamm <momihamm@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/08 18:20:40 by momihamm          #+#    #+#             */
/*   Updated: 2022/11/29 01:22:58 by momihamm         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*niga_itoa(int n, int num_char)
{
	char	*s;

	s = ft_calloc (num_char + 2, sizeof(char));
	if (!s)
		return (NULL);
	s[0] = '-';
	n *= -1;
	while (n > 0)
	{
		s[num_char] = (n % 10) + '0';
		n = (n / 10);
		num_char--;
	}
	return (s);
}

int	countchars(int n)
{
	int	num_char;

	num_char = 0;
	while (n != 0)
	{
		n = n / 10;
		num_char++;
	}
	return (num_char);
}

char	*ft_itoa(int n)
{
	int		num_char;
	int		number;
	char	*s;

	number = n;
	if (n == 0)
		return (ft_strdup("0"));
	else if (n == -2147483648)
		return (ft_strdup("-2147483648"));
	num_char = countchars(n);
	if (number < 0)
		return (niga_itoa(number, num_char));
	s = ft_calloc (num_char + 1, sizeof (char));
	if (!s)
		return (NULL);
	while (number > 0)
	{
		s[--num_char] = (number % 10) + '0';
		number = (number / 10);
	}
	return (s);
}
