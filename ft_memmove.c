/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: momihamm <momihamm@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/13 09:49:17 by momihamm          #+#    #+#             */
/*   Updated: 2022/11/20 17:15:46 by momihamm         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *to, const void *from, size_t byte)
{
	char	*dst;
	char	*src;

	if (!to && !from)
		return (NULL);
	dst = (char *) to;
	src = (char *) from;
	if (dst < src)
		ft_memcpy(dst, src, byte);
	else
	{
		while (byte--)
		{
			dst[byte] = src[byte];
		}	
	}
	return (dst);
}
