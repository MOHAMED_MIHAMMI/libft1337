/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: momihamm <momihamm@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/14 16:26:00 by momihamm          #+#    #+#             */
/*   Updated: 2022/11/30 21:32:53 by momihamm         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	i;
	size_t	j;
	size_t	len_of_dst;

	if ((!dst && size == 0) || (size == 0))
		return (ft_strlen(src));
	len_of_dst = ft_strlen(dst);
	i = ft_strlen(dst);
	j = 0;
	if (size < ft_strlen(dst))
		return (ft_strlen(src) + size);
	while (i < size - 1 && src[j] != '\0')
		dst[i++] = src[j++];
	dst[i] = '\0';
	return (ft_strlen(src) + len_of_dst);
}
