/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: momihamm <momihamm@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/24 18:02:21 by momihamm          #+#    #+#             */
/*   Updated: 2022/11/29 01:25:32 by momihamm         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *s1, char const *set)
{
	char	*new_s1;
	char	*result;
	int		indxof_f;
	int		indxof_b;

	if (!s1 || !set)
		return (NULL);
	new_s1 = ft_strdup (s1);
	if (!new_s1)
		return (NULL);
	indxof_f = 0;
	if (!set)
		return (new_s1);
	while (new_s1[indxof_f] && ft_strchr(set, new_s1[indxof_f]))
		indxof_f++;
	indxof_b = ft_strlen (s1) - 1;
	if (!set)
		return (new_s1);
	while (indxof_b >= 0 && ft_strchr (set, new_s1[indxof_b]))
		new_s1[indxof_b--] = '\0';
	result = ft_strdup (&new_s1[indxof_f]);
	return (free(new_s1), result);
}
