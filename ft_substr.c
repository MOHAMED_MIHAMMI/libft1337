/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_substr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: momihamm <momihamm@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/23 16:35:10 by momihamm          #+#    #+#             */
/*   Updated: 2022/11/29 16:19:10 by momihamm         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_substr(char const *s, unsigned int start, size_t len)
{
	char	*alloc_adds;
	size_t	str_len;
	size_t	indx;
	size_t	end;

	if (!s)
		return (NULL);
	str_len = ft_strlen (s);
	if (start >= str_len)
		return (ft_calloc (1, sizeof(char)));
	if (len > ft_strlen(&s[start]))
		len = ft_strlen(&s[start]);
	alloc_adds = (char *) ft_calloc(len + 1, sizeof (char));
	if (!alloc_adds)
		return (NULL);
	indx = 0;
	end = start + len;
	while (start < end && indx < len + 1 && s[start])
		alloc_adds[indx++] = s[start++];
	return (alloc_adds);
}
