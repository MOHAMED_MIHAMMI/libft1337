/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: momihamm <momihamm@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/20 19:17:00 by momihamm          #+#    #+#             */
/*   Updated: 2022/11/30 21:49:21 by momihamm         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *big, const char *little, size_t len)
{
	size_t	indx_big;
	size_t	indx_lit;

	if (*little == '\0')
		return ((char *)big);
	indx_big = 0;
	indx_lit = 0;
	while (big[indx_big] && indx_big < len)
	{
		while (little[indx_lit] && big[indx_big + indx_lit] == little[indx_lit]
			&& (indx_big + indx_lit) < len)
			indx_lit++;
		if (little[indx_lit] == '\0')
			return ((char *) &big[indx_big]);
		indx_big++;
		indx_lit = 0;
	}
	return (NULL);
}
