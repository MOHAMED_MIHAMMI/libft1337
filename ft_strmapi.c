/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: momihamm <momihamm@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/09 18:42:16 by momihamm          #+#    #+#             */
/*   Updated: 2022/11/30 21:43:00 by momihamm         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	char	*str;
	int		indx;

	if (!s || !f)
		return (NULL);
	str = (char *)malloc (ft_strlen (s) + 1);
	if (!str)
		return (NULL);
	indx = 0;
	while (s[indx])
	{
		str[indx] = (*f)(indx, s[indx]);
		indx++;
	}
	str[indx] = '\0';
	return (str);
}
