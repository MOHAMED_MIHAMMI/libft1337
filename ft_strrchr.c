/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: momihamm <momihamm@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/19 12:12:24 by momihamm          #+#    #+#             */
/*   Updated: 2022/11/20 16:53:08 by momihamm         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	int	indx;

	indx = ft_strlen((char *)s);
	while (indx >= 0)
	{
		if (s[indx] == (char)c)
			return ((char *) &s[indx]);
		indx--;
	}
	return (NULL);
}
