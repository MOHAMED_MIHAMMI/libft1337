/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: momihamm <momihamm@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/19 21:15:50 by momihamm          #+#    #+#             */
/*   Updated: 2022/11/30 21:39:12 by momihamm         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	unsigned char	*casse;
	size_t			indx;

	casse = (unsigned char *)s;
	indx = 0;
	while (indx < n)
	{
		if (casse[indx] == (unsigned char)c)
			return (casse + indx);
		indx++;
	}
	return (NULL);
}
