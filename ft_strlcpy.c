/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: momihamm <momihamm@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/15 22:28:33 by momihamm          #+#    #+#             */
/*   Updated: 2022/11/19 11:49:15 by momihamm         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcpy(char *dst, const char *src, size_t size)
{
	size_t	indx;

	indx = 0;
	if (size != 0)
	{
		while (indx < size -1 && src[indx])
		{
				dst[indx] = src[indx];
				indx++;
		}
		dst[indx] = '\0';
	}
	return (ft_strlen(src));
}
